#!/usr/bin/env sh
cd pyc
sudo -u www-data PYTHONPATH=/var/www/pyc.raylu.net:/var/www/pyc.raylu.net/pyc:/var/www/pyc.raylu.net/submissions DJANGO_SETTINGS_MODULE=pyc.settings django-admin runfcgi socket=/var/run/lighttpd/wsgi.socket method=threaded
