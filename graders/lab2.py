#!/usr/bin/env python

import sys
sys.path.append('/home/dotcloud/env/lib/python2.6/site-packages')
from sandbox import Sandbox, SandboxConfig
from os import path

importname = sys.argv[1]
if importname.endswith('.py'):
	importname = importname[:len(importname)-3]
else:
	print >> sys.stderr, 'Filename did not end with .py for some reason...'
	sys.exit(2)

sys.path.insert(0, path.expanduser('~/submissions'))
sys.dont_write_bytecode = True

output = ''
def run():
	submission = __import__(importname)
	global output
	score = 0

	list1 = [-9, 20, 501]
	list2 = xrange(100)
	if submission.total(list1) == sum(list1):
		score += 1
	else:
		output += "Tried total(%s) and got %s\n" % (list1, submission.total(list1))
	if submission.total(list2) == sum(list2):
		score += 1
	else:
		output += "Tried total(%s) and got %s\n" % (list2, submission.total(list2))

	onestring1 = submission.onestring()
	onestring2 = submission.onestring()
	if onestring1 == 1:
		score += 1
	else:
		output += "First call to onestring() returned %s\n" % onestring1
	if onestring2 == 'ring':
		score += 1
	else:
		output += "Second call to onestring() returned %s\n" % onestring2
	return score

config = SandboxConfig()
config.timeout = 5
config.allowModule(importname, 'total', 'onestring')
sandbox = Sandbox(config)
score = sandbox.call(run)
print score
print >> sys.stderr, output
