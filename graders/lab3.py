#!/usr/bin/env python

import sys
sys.path.append('/home/dotcloud/env/lib/python2.6/site-packages')
from sandbox import Sandbox, SandboxConfig
import os, traceback

importname = sys.argv[1]
if importname.endswith('.py'):
	importname = importname[:len(importname)-3]
else:
	print >> sys.stderr, 'Filename did not end with .py for some reason...'
	sys.exit(2)

os.chdir(sys.path[0])
sys.path.insert(0, os.path.expanduser('~/submissions'))
sys.dont_write_bytecode = True

from random import seed, randint
seed(1)
def wins_best_record(teams, games):
	wins = [[0] * teams for _ in range(teams)]
	for i in xrange(teams):
		for j in xrange(teams):
			wins[i][j] = randint(0, games)
			wins[j][i] = games - wins[i][j]
	sums = map(sum, wins)
	largest_idx = 0
	for i in xrange(1, len(sums)):
		if sums[i] > sums[largest_idx]:
			largest_idx = i
	return wins, largest_idx, sums[largest_idx]

pizfiles = [('handout.piz', 3950), ('invalid.piz', 1050)]

output = ''
score = 0
def run():
	submission = __import__(importname)
	global output, score

	pizzas = [
			{'num_toppings': 1, 'toppings': [('topping', 0)], 'size': 10},
			{'num_toppings': 2, 'toppings': [('one', 1), ('two', 2)], 'size': 12},
			{'num_toppings': 0, 'toppings': [], 'size': 8},
			{'num_toppings': 1, 'toppings': [('topping', 0)], 'size': 7},
			{'num_toppings': 2, 'toppings': [('topping one', 1), ('topping two', 1)], 'size': 6},
			{'num_toppings': 3, 'toppings': [('one', 2), ('two', 1), ('three', 0)], 'size': 12},
			{'toppings': [('', 0)], 'size': 10},
			{'num_toppings': 1, 'toppings': [('topping', 0)]},
		]
	validity = [True, True, False, False, False, True, False, False]
	for i in range(len(pizzas)):
		if submission.valid_pizza(pizzas[i]) == validity[i]:
			score += 1
		else:
			output += '%s should be %s\n' % (pizzas[i], 'valid' if validity[i] else 'invalid')

	wbr_args = [(5, 3), (5, 3), (16, 5)]
	for args in wbr_args:
		wins, b_team, b_number = wins_best_record(*args)
		team, number = submission.best_record(wins)
		if team == b_team:
			score += 1
		else:
			output += 'Best record of\n'
			for l in wins:
				output += '    %s\n' % l
			output += '    should be team #%d\n' % b_team
		if number == b_number:
			score += 1
		else:
			output += 'Best record of\n'
			for l in wins:
				output += '    %s\n' % l
			output += '    should be %d wins\n' % b_number

	for pizfile in pizfiles:
		result = submission.order_pizza(pizfile[0])
		if result == pizfile[1]:
			score += 3
		else:
			output += 'Order was incorrect for %s; got %d (answer is hidden)\n' % (pizfile[0], result)

	return score

config = SandboxConfig()
config.timeout = 5
config.allowModule(importname, 'valid_pizza', 'best_record', 'order_pizza')
for pizfile in pizfiles:
	config.allowPath(pizfile[0])
sandbox = Sandbox(config)
try:
	score = sandbox.call(run)
except:
	traceback.print_exc(20, sys.stderr)
	print >> sys.stderr
print score
print >> sys.stderr, output
