#!/usr/bin/env python

handout = [
		{'toppings': [('pepperoni', 0)], 'size': 12},
		{
				'toppings': [
					('tomato', 1),
					('sauce', 2),
					],
				'size': 8
				},
		{
				'toppings': [
					('some stuff', 0),
					('other stuff', 1),
					('even more stuff', 1),
					],
				'size': 10
				},
		{
				'toppings': [
					('tomato', 1),
					('sauce', 2),
					('', 0),
					('food', 0),
					],
				'size': 6
			},
	]
invalid = [
		{'toppings': [('pepperoni', 0)], 'size': 10},
		{'toppings': [('biribiri', 0)]},
		{'size': 6},
		{'toppings': [('pepperoni', 0)], 'size': 9},
		{'toppings': [('cheese', 1)], 'size': 12},
	]

def write_pizza(f, pizza):
	if 'toppings' in pizza:
		for topping in pizza['toppings']:
			f.write('%s,%d,' % (topping[0], topping[1]))
	if 'size' in pizza:
		f.write(str(pizza['size']))
	f.write('\n')

def write_pizfile(filename, pizzas):
	f = open(filename, 'w')
	for pizza in pizzas:
		write_pizza(f, pizza)
	f.close()

write_pizfile('handout.piz', handout)
write_pizfile('invalid.piz', invalid)
