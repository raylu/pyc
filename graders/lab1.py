#!/usr/bin/env python

import sys
sys.path.append('/home/dotcloud/env/lib/python2.6/site-packages')
from sandbox import Sandbox, SandboxConfig
from os import path

importname = sys.argv[1]
if importname.endswith('.py'):
	importname = importname[:len(importname)-3]
else:
	print >> sys.stderr, 'Filename did not end with .py for some reason...'
	sys.exit(2)

sys.path.insert(0, path.expanduser('~/submissions'))
sys.dont_write_bytecode = True

def run():
	submission = __import__(importname)
	if submission.test() == 1:
		return 100
	else:
		return 0

config = SandboxConfig()
config.timeout = 5
config.allowModule(importname, 'test')
sandbox = Sandbox(config)
score = sandbox.call(run)
print score
