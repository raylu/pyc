#!/usr/bin/env python

# Normally, there won't be two assignments overlapping, but we're still testing
# so whatever. You may wish to look to http://effbot.org/zone/python-list.htm
# for help, or the faster-paced http://docs.python.org/tutorial/controlflow.html.
# Also, http://www.saltycrane.com/blog/2008/01/python-variable-scope-notes/ may
# be of use. Note that I do not recommend anything but docs.python.org as a
# general reference.

# total should return the sum of the numbers passed in
def total(numbers):
    return numbers[0]

# onestring should return 1 the first time it's called and 'ring' the second time
def onestring():
    return 1

if __name__ == '__main__':
    list1 = [1, 2, 3]
    list2 = [-10, -10, -30]
    print 'sum of list1:', total(list1)
    print 'sum of list2:', total(list2)

    print # blank line

    if onestring() == 1:
        print 'got 1 the first time!'
    else:
        print 'onestring didn\'t return 1 the first time :('
    if onestring() == 'ring':
        print "got 'ring' the second time!"
    else:
        print "onestring didn't print 'ring' the second time D:"
