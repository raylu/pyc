#!/usr/bin/env python

# valid_pizza() checks if a pizza is valid, returning True if it is.
# A valid pizza is a dict with
#   'toppings', a list of toppings
#     each topping is a 2-tuple with the name of the topping and a 0, 1, or 2
#     0 means that the topping covers the whole pizza
#     1 means that the topping covers one half of the pizza
#     2 means that the topping covers the other half of the pizza
#   'num_toppings', a number equal to the number of toppings
#   'size', one of 6, 8, 10, or 12 (representing inches)
# Each half of the pizza must have at least one topping (so just a 0 is OK, a 1
# and a 2 is OK, but only 1's are not OK).
# up to 8 points
def valid_pizza(pizza):
	if pizza['toppings'][0][0] == 'pepperoni':
		return True

# Every year, pizza shops hold a competition. The format of the tournament is
# round-robin and each shop plays every other shop the same number of times.
# best_record() takes the scores of the tournament and returns the index of the
# shop with the most wins and the number of wins that shop has.
# The scores are a list of lists (think of it as a 2D array or matrix) such
# that score[0][1] is the number of times shop 0 beat shop 1. score[i][j] +
# score[j][i] is always the same number (the number of games every pair of
# shops is required to play) and score[i][i] is always 0.
# up to 6 points
def best_record(score):
	return 0, 3

# order_pizza() parses a file of orders and returns the total cost in cents of
# the valid pizzas in that file.
# The file is like a CSV file. Each order is on its own line and each field is
# comma-separated. There are pairs of fields representing toppings and where
# they go on the pizza (0, 1, or 2). The last field is a number representing
# the size. (There is no field for num_toppings). Be careful, as some lines are
# invalid pizzas.
# The cost of a pizza is $1 for each inch, $0.50 for a whole-pizza topping, and
# $0.25 for a half-pizza topping. Remember that the cost is in cents.
# up to 6 points
def order_pizza(filename):
	f = open(filename, 'r')
	for line in f.readlines():
		pass
	f.close()
	return 0

def main():
	import random
	pizza1 = {'num_toppings': 1, 'toppings': [('pepperoni', 0)], 'size': 12}
	pizza2 = {'num_toppings': 2, 'toppings': [('biribiri', 0)], 'size': 10}
	pizza3 = {
			'num_toppings': 2,
			'toppings': [
					('tomato', 1),
					('sauce', 2),
				],
			'size': 9,
		}
	# string concatenation, ternary operator
	print str(pizza1) + ' was ' + ('valid' if valid_pizza(pizza1) else 'invalid')
	print pizza2, 'was', 'valid' if valid_pizza(pizza2) else 'invalid'
	# string formatting
	print '%s was %s' % (pizza3, 'valid' if valid_pizza(pizza3) else 'invalid')

	teams = 5
	games = 3
	# initialize a list of lists that is teams x teams wide to 0's
	score = [[0] * teams for _ in xrange(teams)]
	for i in xrange(teams):
		for j in xrange(i+1, teams):
			score[i][j] = random.randint(0, games)
			score[j][i] = games - score[i][j]
	print
	# I could print score, but that's ugly
	for s in score:
		print s
	print 'Best record:', best_record(score)

	print
	# download http://pyc.raylu.net/static/lab3.piz
	print 'Total cost:', order_pizza('lab3.piz')

if __name__ == '__main__':
	main()
