#!/usr/bin/env python

# Welcome to the test of pyc! Thanks for participating.
# The first assignment is to create a function called 'test' that returns 1
# This is probably easier than you think.

def test():
    return 0

if __name__ == '__main__':
    print test()
