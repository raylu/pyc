from django.db import models

class Lab(models.Model):
	name = models.CharField(max_length=64)
	due = models.DateField()
	def __unicode__(self):
		return self.name

class Submission(models.Model):
	from django.contrib.auth.models import User
	def upload_filename(instance, filename):
		from time import time
		return "s%d_%d_%d.py" % (instance.lab.id, instance.user.id, int(time()))
	lab = models.ForeignKey(Lab)
	user = models.ForeignKey(User)
	time = models.DateTimeField()
	file = models.FileField(upload_to=upload_filename)
	grade = models.IntegerField(null=True)
	class Meta:
		unique_together = (('lab', 'user'),)
	def __unicode__(self):
		return "%d, %s, %s" % (self.lab.id, self.user, self.grade)
