from grader.models import Lab, Submission
from django.contrib import admin

admin.site.register(Lab)
admin.site.register(Submission)
