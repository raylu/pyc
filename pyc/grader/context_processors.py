def nav(request):
	from grader.models import Lab
	return {
			'labs': Lab.objects.all(),
			'authenticated' : request.user.is_authenticated()
			}
