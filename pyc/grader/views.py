from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import login_required, user_passes_test
from django.conf import settings

from grader.models import Lab, Submission

import os

def index(request):
	return render_to_response('index.html',
			context_instance=RequestContext(request))

def password_changed(request):
	return render_to_response('password_changed.html',
			context_instance=RequestContext(request))

def lab(request, lab_id):
	from datetime import timedelta
	lab = Lab.objects.get(pk=lab_id)

	submissions = lab.submission_set
	if not request.user.is_staff:
		# only staff can see other staff solutions
		submissions = submissions.filter(user__is_staff=False)
	submissions = submissions.order_by('-grade', 'time')

	# times are stored in UTC; do the conversion here
	tzoffset = timedelta(0, 0, 0, 0, 0, -7) # -7 hours
	for s in submissions:
		s.time += tzoffset

	return render_to_response('lab.html', {
			'lab' : lab,
			'submissions' : submissions,
			'staff' : request.user.is_staff
			}, context_instance=RequestContext(request))

@login_required
def submit(request, lab_id):
	from django.http import HttpResponseRedirect
	from django.db import IntegrityError
	from datetime import datetime

	if request.method == 'POST':
		form = SubmissionForm(request.POST, request.FILES)
		if form.is_valid():
			labobj = Lab.objects.get(pk=lab_id)
			rfile = request.FILES['file']
			submission = Submission(lab=labobj, user=request.user, time=datetime.utcnow())
			try:
				submission.save()
			except IntegrityError:
				# already exists
				submission = Submission.objects.get(lab__exact=lab_id, user__exact=request.user)
				if submission.file:
					os.unlink(os.path.join(settings.MEDIA_ROOT, submission.file.name))
			submission.file = rfile
			submission.save() # this moves the file to MEDIA_ROOT
			score, err = grade(submission.file.name, lab_id)
			try:
				score = int(score)
			except ValueError:
				score = 0
			if score > submission.grade:
				submission.grade = score
				submission.time = datetime.utcnow()
			submission.save()

			return render_to_response('submit.html', {
					'err' : err,
					'score' : score,
					'form' : form,
					'lab_id' : lab_id,
					}, context_instance=RequestContext(request))
	else:
		form = SubmissionForm()
	return render_to_response('submit.html', {
			'form' : form,
			'lab_id' : lab_id,
			}, context_instance=RequestContext(request))

@user_passes_test(lambda u: u.is_staff)
def submission(request, lab_id, user_id):
	submission = Submission.objects.get(lab__exact=lab_id, user__exact=user_id)
	filepath = os.path.join(settings.MEDIA_ROOT, submission.file.name)
	f = open(filepath, 'r')
	code = f.read()
	f.close()
	return render_to_response('submission.html', {
			'code' : code,
			}, context_instance=RequestContext(request))

def faq(request):
	return render_to_response('faq.html', {},
			context_instance=RequestContext(request))

def grade(file, id):
	import subprocess
	from os import path
	graderfile = "lab%s.py" % id
	graderdir = path.dirname(__file__) + '/../../graders/'
	grader = graderdir + graderfile
	p = subprocess.Popen([grader, file], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	return p.communicate()

from django import forms
class SubmissionForm(forms.Form):
	file = forms.FileField()
