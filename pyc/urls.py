from django.conf.urls.defaults import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
	url(r'^$', 'grader.views.index', name='index'),
	url(r'^lab/(?P<lab_id>\d+)/$', 'grader.views.lab'),
	url(r'^submit/(?P<lab_id>\d+)/$', 'grader.views.submit'),
	url(r'^submission/(?P<lab_id>\d+)/(?P<user_id>\d+)/$', 'grader.views.submission'),
	url(r'^faq/$', 'grader.views.faq'),

	url(r'^login/$', 'django.contrib.auth.views.login', {'template_name': 'login.html'}),
	url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'}),
	url(r'^password/$', 'django.contrib.auth.views.password_change', {'template_name': 'password.html', 'post_change_redirect': '/password_changed'}),
	url(r'^password_changed/$', 'grader.views.password_changed'),

	# Uncomment the admin/doc line below to enable admin documentation:
	# url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

	url(r'^admin/', include(admin.site.urls)),
)
